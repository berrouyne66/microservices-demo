# Build adservice
FROM python:3.7-slim AS adservice-build
WORKDIR /app
COPY src/adservice .
RUN pip install --no-cache-dir -r requirements.txt

# Build artservice
FROM node:12-slim AS artservice-build
WORKDIR /app
COPY src/artservice .
RUN npm install

# Build checkoutservice
FROM python:3.7-slim AS checkoutservice-build
WORKDIR /app
COPY src/checkoutservice .
RUN pip install --no-cache-dir -r requirements.txt

# Build currencyservice
FROM python:3.7-slim AS currencyservice-build
WORKDIR /app
COPY src/currencyservice .
RUN pip install --no-cache-dir -r requirements.txt

# Build emailservice
FROM python:3.7-slim AS emailservice-build
WORKDIR /app
COPY src/emailservice .
RUN pip install --no-cache-dir -r requirements.txt

# Build frontend
FROM node:12-slim AS frontend-build
WORKDIR /app
COPY src/frontend .
RUN npm install && npm run build

# Build loadgenerator
FROM python:3.7-slim AS loadgenerator-build
WORKDIR /app
COPY src/loadgenerator .
RUN pip install --no-cache-dir -r requirements.txt

# Build paymentservice
FROM python:3.7-slim AS paymentservice-build
WORKDIR /app
COPY src/paymentservice .
RUN pip install --no-cache-dir -r requirements.txt

# Build productcatalogservice
FROM python:3.7-slim AS productcatalogservice-build
WORKDIR /app
COPY src/productcatalogservice .
RUN pip install --no-cache-dir -r requirements.txt

# Build recommendationservice
FROM python:3.7-slim AS recommendationservice-build
WORKDIR /app
COPY src/recommendationservice .
RUN pip install --no-cache-dir -r requirements.txt

# Build shippingservice
FROM python:3.7-slim AS shippingservice-build
WORKDIR /app
COPY src/shippingservice .
RUN pip install --no-cache-dir -r requirements.txt

# Final stage: Create an image with all services
FROM alpine:latest
COPY --from=adservice-build /app /services/adservice
COPY --from=artservice-build /app /services/artservice
COPY --from=checkoutservice-build /app /services/checkoutservice
COPY --from=currencyservice-build /app /services/currencyservice
COPY --from=emailservice-build /app /services/emailservice
COPY --from=frontend-build /app /services/frontend
COPY --from=loadgenerator-build /app /services/loadgenerator
COPY --from=paymentservice-build /app /services/paymentservice
COPY --from=productcatalogservice-build /app /services/productcatalogservice
COPY --from=recommendationservice-build /app /services/recommendationservice
COPY --from=shippingservice-build /app /services/shippingservice
